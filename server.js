const express = require('express')
const app = express()
const port = 3000

require('what-input');
require('jquery');
require('ejs');

app.use(express.static(__dirname + '/public'));

app.set('view engine', 'ejs');


app.get('/', (req, res) => {
  res.render('index')
})

app.get('/report', (req, res) => {
  res.render('pages/report')
})


app.listen(port, () => {
  console.log(`Example app listening at http://localhost:${port}`)
})